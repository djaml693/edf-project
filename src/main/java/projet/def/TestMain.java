package projet.def;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.indices.CreateIndex;

public class TestMain {

	final static Logger logger = Logger.getLogger(TestMain.class);
	
	public static void main(String[] args) throws ParseException, IOException, InterruptedException {
		// TODO Auto-generated method stub
		Instant start = Instant.now();
		/* ... the code being measured starts ... */

		// sleep for 5 seconds
		TimeUnit.SECONDS.sleep(60);

		/* ... the code being measured ends ... */

		Instant end = Instant.now();

		Duration interval = Duration.between(start, end);

		System.out.println("Execution time in seconds: " +
								interval.getSeconds());
		
		
		
		
		
		
		
		
		
		
		
		/*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy H:mm");
		LocalDateTime dateReception = LocalDateTime.parse("02/12/2019 14:58", formatter);
		
		System.out.println(dateReception.plusDays(1));
		
		  SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
		    String strDate= formatter.format(dateReception);
		    
		    //System.out.println(strDate);*/
		
		//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        //System.out.println("outputFile-"+timestamp.toString().substring(0,16).replace(" ","T"));
		
		
		
		
		//Date date1 = new Date();
		
		//System.out.println(date1);
		
		
	}
	
	
public static boolean isWeekend(LocalDateTime date) {
		
		//Calendar c = Calendar.getInstance();
		//c.setTime(date);
		//int day = c.get(Calendar.DAY_OF_WEEK);
		String dat = date.getDayOfWeek().toString();
		System.out.println(dat);
		if(dat.toUpperCase().equals("SATURDAY") || dat.toUpperCase().equals("SUNDAY") ) {
			
			return true;
					
		}else {
			
			return false;
		}		
		
	}
}
