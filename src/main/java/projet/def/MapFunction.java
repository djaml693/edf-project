package projet.def;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

/**
 * Class of map function.
 * 
 * @author Djamel CHEROUFA
 * @version 1.0.0, 01/10/2020
 */

public class MapFunction {

	/**
	 * This method is a map who check if the commitment is respected.
	 * 
	 * @param s String line of JavaRDD.
	 * @param list List<String> contain list of holidays.
	 * @return this method save data in Elasticsearch and return a dataset Row.
	 * @since version 1.0
	 */

	public static Row calculEngagement(String s, List<String> list) throws IOException {

		String tab[] = s.split(";");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy H:mm");
		LocalDateTime dateReception = LocalDateTime.parse(tab[2], formatter);
		LocalDateTime dateEnvoi = LocalDateTime.parse(tab[3], formatter);

		String flag = "";
		Long diff = Functions.checkDifference(dateReception, dateEnvoi);

		if (dateReception.getHour() <= 12 && (dateReception.getDayOfWeek() == dateEnvoi.getDayOfWeek())) {

			flag = "oui";

		} else {
			
			if(dateReception.getHour() <= 12 && (dateReception.getDayOfWeek() != dateEnvoi.getDayOfWeek())){
				
				flag = "non";
				
			}else {

			if (dateReception.getHour() > 12 && dateReception.getDayOfWeek().plus(1) == dateEnvoi.getDayOfWeek()) {

				flag = "oui";

			} else {

				for (int i = 1; i < diff; i++) {

					if (Functions.isHoliday(dateReception.plusDays(i), list) == true
							|| Functions.isWeekend(dateReception.plusDays(i)) == true) {

						flag = "oui";

					} else {

						flag = "non";
						break;
					}
				}
				}

			}
		}

		HashMap<String, String> map = new HashMap<String, String>();
		// Map<String, Object> employeeHashMap = new LinkedHashMap<>();
		map.put("id_doc", tab[0]);
		map.put("id_depot", tab[1]);
		map.put("date_validation_depot", tab[2]);
		map.put("date_remise_poste", tab[3]);
		map.put("delai_respect_sla", tab[4]);
		map.put("delai_traitement", diff.toString());
		map.put("flag_respect_sla", flag);

		//Functions.sendData(map);

		return RowFactory.create(

				tab[0].toString(), tab[1].toString(), tab[2].toString(), tab[3].toString(), tab[4].toString(),
				diff.toString(), flag.toString());

	}

}
