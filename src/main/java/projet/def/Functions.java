package projet.def;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.IndicesExists;

/**
 * This class contain all methods.
 * 
 * @author Djamel CHEROUFA
 * @version 1.0.0, 01/10/2020
 */
public class Functions {

	final static Logger logger = Logger.getLogger(TestMain.class);

	/**
	 * This method will set value for each JobConf variables.
	 * 
	 * @param args String array contain all input parameters of the program.
	 * @since version 1.1.0
	 */
	static JobConff jobConf = new JobConff();

	public static JobConff setValues(String[] args) {

		for (String s : args) {

			String[] tab = s.split("=");
			System.out.println("tab[0] =" + tab[0]);
			System.out.println("tab[1] =" + tab[1]);

			switch (tab[0]) {

			case "pathCsvInput":
				jobConf.setPathCsvInput(tab[1]);
				break;

			case "pathCsvHolidays":
				jobConf.setPathCsvHolidays(tab[1]);
				break;

			case "separatorCsv":
				jobConf.setSeparatorCsv(tab[1]);
				break;

			case "pathCsvOutput":
				jobConf.setPathCsvOutput(tab[1]);
				break;

			case "urlElk":
				jobConf.setUrlElk(tab[1]);
				break;

			case "portElk":
				jobConf.setPortElk(tab[1]);
				break;

			case "indexElk":
				jobConf.setIndexElk(tab[1]);
				break;

			default:
				break;

			}
		}

		return jobConf;
	}

	/**
	 * This method will check if a day is holiday.
	 * 
	 * @param date LocalDate value to be checked.
	 * @param list List<String> contain list of holidays.
	 * @return this method return a boolean true if day is holiday else false.
	 * @since version 1.0
	 */
	public static Boolean isHoliday(LocalDateTime date, List<String> list) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy H:mm");
		String strDate = formatter.format(date).substring(0, 10).replace("/", "-");

		return list.contains(strDate);
	}

	/**
	 * This method will check if a day is weekend.
	 * 
	 * @param date LocalDate value to be checked.
	 * @return this method return a boolean true if day is weekend else false.
	 * @since version 1.0
	 */

	public static boolean isWeekend(LocalDateTime date) {

		String dat = date.getDayOfWeek().toString();

		if (dat.toUpperCase().equals("SATURDAY") || dat.toUpperCase().equals("SUNDAY")) {

			return true;

		} else {

			return false;
		}

	}

	/**
	 * This method will calculate difference between two dates.
	 * 
	 * @param date1 LocalDate value of reception date.
	 * @param date2 LocalDate value of sending date.
	 * @return this method return number of days difference between the two dates.
	 * @since version 1.0
	 */

	public static Long checkDifference(LocalDateTime date1, LocalDateTime date2) {

		// Parsing the date
		LocalDate dateReception = LocalDate.parse(date1.toString().substring(0, 10));
		LocalDate dateEnvoi = LocalDate.parse(date2.toString().substring(0, 10));

		// calculating number of days in between
		long noOfDaysBetween = ChronoUnit.DAYS.between(dateReception, dateEnvoi);

		return noOfDaysBetween;

	}

	/**
	 * This method save data in Elasticsearch.
	 * 
	 * @param map HashMap contain document to save in Elasticsearch.
	 * @throws Exception in case failed connection to Elasticsearch.
	 * @since version 1.0
	 */

	public static void sendData(HashMap map) throws IOException {

		// JobConff jobConf = new JobConff();

		try {

			JestClient jestClient = jestClient();

			logger.info(" successful connection to Elasticsearch ");

			if (indexExist(JobConf.indexElk) == false) {

				jestClient.execute(new CreateIndex.Builder(jobConf.getIndexElk()).build());

			}

			jestClient.execute(new Index.Builder(map).index(jobConf.getIndexElk()).type("_doc").build());

		} catch (Exception e) {

			logger.error(" Failed to connect to Elasticsearch " + e.getMessage());
		}

	}

	/**
	 * This method create a connection to Elasticsearch.
	 * 
	 * @since version 1.0
	 */

	private static JestClient jestClient() {
		JestClientFactory factory = new JestClientFactory();
		JobConff jobConf = new JobConff();
		factory.setHttpClientConfig(new HttpClientConfig.Builder(jobConf.getUrlElk() + ":" + jobConf.getPortElk())
				.multiThreaded(true).defaultMaxTotalConnectionPerRoute(2).maxTotalConnection(20).build());
		return factory.getObject();
	}

	/**
	 * This method check if index is exist.
	 * 
	 * @param index String name of index.
	 * @return this method return true if index exist else false.
	 * @since version 1.0
	 */
	public static boolean indexExist(String index) throws IOException {

		JestClient jestClient = jestClient();
		JestResult result = jestClient.execute(new IndicesExists.Builder(index).build());

		if (result.isSucceeded()) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * This method create a schema for the dataFrame.
	 * 
	 * @return this method return StructType schema.
	 * @since version 1.0
	 */

	public static StructType schema() {
		List<StructField> schemaFields = new ArrayList<StructField>();
		schemaFields.add(DataTypes.createStructField("id_doc", DataTypes.StringType, true));
		schemaFields.add(DataTypes.createStructField("id_depot", DataTypes.StringType, true));
		schemaFields.add(DataTypes.createStructField("date_validation_depot", DataTypes.StringType, true));
		schemaFields.add(DataTypes.createStructField("date_remise_poste", DataTypes.StringType, true));
		schemaFields.add(DataTypes.createStructField("delai_respect_sla", DataTypes.StringType, true));
		schemaFields.add(DataTypes.createStructField("delai_traitement", DataTypes.StringType, true));
		schemaFields.add(DataTypes.createStructField("flag_respect_sla", DataTypes.StringType, true));

		StructType schema = DataTypes.createStructType(schemaFields);

		return schema;
	}

	/**
	 * This method save data in CSV file.
	 * 
	 * @param ds Dataset<Row> dataset to save.
	 * @since version 1.0
	 */

	public static void saveAsCsvFile(Dataset<Row> ds) {

		// JobConff jobConf = new JobConff();

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStmp = timestamp.toString().substring(0, 16).replace(" ", "T");

		ds.write().mode("append").option("header", "true").option("sep", jobConf.getSeparatorCsv())
				.option("timestampFormat", "yyyy-MM-dd hh:ss:SS").csv(JobConf.pathCsvOutput);

	}

	/**
	 * This method search data in Elasticsearch using query.
	 * 
	 * @param query String query to send to Elasticsearch.
	 * @return this method return data objects.
	 * @since version 1.0
	 */

	public static void serchByQuery(String query) {

		String query1 = "{\"query\": {\"match\": {\"name\": \"Djamel\"}}}";

		Search.Builder search = new Search.Builder(query).addIndex("employees");

		/* SearchResult result1 = jestClient.execute(search.build()); */

	}
}
