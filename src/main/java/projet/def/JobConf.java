/**
 * 
 */
package projet.def;

/**
 * Class of parameters.
 * 
 * @author Djamel CHEROUFA
 * @version 1.0.0, 01/10/2020
 */
public interface JobConf {

	/**
	 * @param pathCsvInput String path of csv input file.
	 * @param pathCsvHolidays String path of csv holidays input file.
	 * @param separatorCsv String separator of csv input files.
	 * @param pathCsvOutput String path of csv output file.
	 */
	String pathCsvInput = "D:/data/input/edf1.csv";
	String pathCsvHolidays = "D:/data/input/jours.csv";
	String separatorCsv = ";";
	String pathCsvOutput = "D:/data/output/";

	/**
	 * @param urlElk String url of Elasticsearch.
	 * @param portElk String Elasticsearch port.
	 * @param indexElk String Elasticsearch index.
	 */
	String urlElk = "http://192.168.0.17";
	String portElk = "80";
	String indexElk = "";

}
