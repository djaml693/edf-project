package projet.def;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * The class main.
 * 
 * @author Djamel CHEROUFA
 * @version 1.0.0, 01/10/2020
 */

public class MainClass {

	final static Logger logger = Logger.getLogger(MainClass.class);

	public static void main(String[] args) {

		/**
		 * Time start execution.
		 */
		Instant start = Instant.now();

		logger.info(
				"********************************************************** Start of execution **********************************************************");

		SparkSession spark = SparkSession.builder().appName("projet-edf").config("spark.driver.memory", "2")
				.config("spark.executor.cores", "2").config("spark.executor.insatances", "1").master("local[*]")
				.getOrCreate();

		logger.info(
				"************************************************* Setting value for each jobConf variable***********************************************");

		/**
		 * Setting value for variables.
		 */
		JobConff jobConff = Functions.setValues(args);

		/**
		 * create a JavaRDD from CSV input file.
		 */
		JavaRDD<String> rdd = spark.read().option("encoding", "UTF-8").option("inferSchema", "false")
				.textFile(jobConff.getPathCsvInput()).toJavaRDD();

		/**
		 * create a list from csv input holidays file.
		 */
		List<String> list = spark.read().option("encoding", "UTF-8").option("inferSchema", "false")
				.textFile(jobConff.getPathCsvHolidays()).collectAsList();

		/**
		 * create a JavaRDD of Row using map function.
		 */
		JavaRDD<Row> ds = rdd.map(f -> MapFunction.calculEngagement(f, list));

		/**
		 * create a dataFrame from dataset ds.
		 */
		Dataset<Row> datafr = spark.createDataFrame(ds, Functions.schema());

		datafr.show();

		/**
		 * save data in CSV File.
		 */
		Functions.saveAsCsvFile(datafr);

		/**
		 * Time end execution.
		 */
		Instant end = Instant.now();

		/**
		 * Execution time in seconds.
		 */
		Duration interval = Duration.between(start, end);

		logger.info("The execution time is : " + interval.getSeconds() + " secondes");

		logger.info(
				"********************************************************** End of execution **********************************************************");

	}

}
