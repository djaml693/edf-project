package projet.def;

/**
 * Class of parameters.
 * 
 * @author Djamel CHEROUFA
 * @version 1.1.0, 01/10/2020
 */
public class JobConff {

	/**
	 * @param pathCsvInput path of csv input file.
	 * @param pathCsvHolidays path of csv holidays input file.
	 * @param separatorCsv separator of csv input files.
	 * @param pathCsvOutput path of csv output file.
	 */
	String pathCsvInput = "D:/data/input/edf1.csv";
	String pathCsvHolidays = "D:/data/input/jours.csv";
	String separatorCsv = ";";
	String pathCsvOutput = "D:/data/output/";

	/**
	 * @param urlElk String url of Elasticsearch.
	 * @param portElk String Elasticsearch port.
	 * @param indexElk String Elasticsearch index.
	 */
	String urlElk = "http://192.168.0.17";
	String portElk = "80";
	String indexElk = "";
	public String getPathCsvInput() {
		return pathCsvInput;
	}
	public void setPathCsvInput(String pathCsvInput) {
		this.pathCsvInput = pathCsvInput;
	}
	public String getPathCsvHolidays() {
		return pathCsvHolidays;
	}
	public void setPathCsvHolidays(String pathCsvHolidays) {
		this.pathCsvHolidays = pathCsvHolidays;
	}
	public String getSeparatorCsv() {
		return separatorCsv;
	}
	public void setSeparatorCsv(String separatorCsv) {
		this.separatorCsv = separatorCsv;
	}
	public String getPathCsvOutput() {
		return pathCsvOutput;
	}
	public void setPathCsvOutput(String pathCsvOutput) {
		this.pathCsvOutput = pathCsvOutput;
	}
	public String getUrlElk() {
		return urlElk;
	}
	public void setUrlElk(String urlElk) {
		this.urlElk = urlElk;
	}
	public String getPortElk() {
		return portElk;
	}
	public void setPortElk(String portElk) {
		this.portElk = portElk;
	}
	public String getIndexElk() {
		return indexElk;
	}
	public void setIndexElk(String indexElk) {
		this.indexElk = indexElk;
	}



}
